import React from 'react';


const Hello = props => {
    return (
        <>
            <div className="container is-mobile">
                <div className="columns has-text-centered" style={{marginTop: '14rem'}}>
                    <div className="column">
                        <h1 className="is-size-1 has-text-weight-light">Hello, React world!</h1>
                        <h5 className="is-size-6 has-text-weight-medium is-family-code">🚀 We are coming...</h5>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Hello