# [kiwanda](https://kiwanda.org/), hello world test

This is an `Hello World` application, assigned by <strong>kiwanda</strong>'s instructor as first test on our journey to become <strong>React JS Experts</strong>. Hosted on firebase at [kiwanda-hello-leoka](https://kiwanda-hello-leoka.web.app/).

## Technologies
- React v17
- Bulma v0.9.1
- Firebase (Hosting)

## Credit
Happy coding! 🔥 